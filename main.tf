provider "aws" {
access_key = "${var.aws_access_key}"
secret_key = "${var.aws_secret_key}"
region = "${var.aws_region}"
}

resource "aws_s3_bucket" "onebucket" {
   bucket = "testing-s3-with-terraform-sachin8989"
   acl    = "private"
   versioning {
      enabled = true
   }
   tags = {
     Name = "Bucket1-sachin"
     Environment = "Test"
   }
}
